<?php
namespace App\Controllers;
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class Module extends BaseController
{
    public function premix()
    {
        return view('module/premix');
    }

    public function businesspartner()
    {
        return view('module/businesspartner/businesspartner');
    }
}
