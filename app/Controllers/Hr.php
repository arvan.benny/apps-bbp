<?php

namespace App\Controllers;
use App\Models\UsersModel;
use App\Models\MeetingRoomModel;

class Hr extends BaseController
{
    protected $userModel;
    protected $meetingRoomModel;
    public function __construct()
    {
        $this->userModel = new UsersModel();
        $this->meetingRoomModel = new MeetingRoomModel();
    }
    public function index()
    {
        
        $users = $this->userModel->allUser();
        $data = [
            'title' => 'BBP | User List',
            'users' => $users
        ];
        return view('hr/index', $data);

    }

    public function meetingroom()
    {
        $getmeetroom = $this->meetingRoomModel->apprvschedule();
        $data = [
            'meetroom' => $getmeetroom
        ];
        // dd($data);
        return view('hr/meetingroom', $data);
    }
}