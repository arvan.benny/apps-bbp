<?php

namespace App\Controllers;
use App\Models\UsersModel;
use App\Models\UserAccessModel;

class Admin extends BaseController
{
    protected $userModel;
    protected $userAccess;
    public function __construct()
    {
        $this->userModel = new UsersModel();
        $this->userAccess = new UserAccessModel();
    }
    public function index()
    {
        
        $users = $this->userModel->allUser();
        $data = [
            'title' => 'BBP | User List',
            'users' => $users
        ];
        return view('admin/index', $data);

    }

    public function detail($id = null)
    {
        
        $users = $this->userModel->getUser($id);
        $role = $this->userModel->getRole($id);
        $allaccess = $this->userAccess->allgroup();
        $access = $this->userAccess->access($id);
        $data = [
            'title' => 'BBP | User Detail',
            'h1' => 'User Detail',
            'users' => $users,
            'role' => $role,
            'allaccess' => $allaccess,
            'access' => $access
        ];
        // dd($access);
        return view('admin/detail',$data);
    }


}
