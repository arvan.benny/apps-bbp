<?php

namespace App\Controllers;
use App\Models\UsersModel;

class User extends BaseController
{
    protected $userModel;
    public function __construct()
    {
        $this->userModel = new UsersModel();
    }
    public function index()
    {
        return view('user/index');
    }

    public function profile($id = null)
    {
        $users = $this->userModel->getUser($id);
        $role = $this->userModel->getRole($id);
        $data = [
            'title' => 'BBP | User Detail',
            'h1' => 'User Detail',
            'users' => $users,
            'role' => $role
        ];
        return view('user/profile',$data);
    }
}
