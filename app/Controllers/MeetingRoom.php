<?php

namespace App\Controllers;

use App\Models\MeetingRoomModel;
use App\Models\UsersModel;
use App\Models\ParticipantModel;
use App\Models\NotulenModel;
// use Myth\Auth\Models\UserModel;

class MeetingRoom extends BaseController
{
    protected $meetingRoomModel;
    protected $userModel;
    protected $participantModel;
    protected $notulenModel;
    public function __construct()
    {
        $this->meetingRoomModel = new MeetingRoomModel();
        $this->usersModel = new UsersModel();
        $this->participantModel = new ParticipantModel();
        $this->notulenModel = new NotulenModel();
    }
    public function index($id = null)
    {
        $id = user_id();
        $getmeetroom = $this->meetingRoomModel->getmeetroom($id);
        $ongoing = $this->meetingRoomModel->ongoingschedule($id);
        $notif = $this->meetingRoomModel->notifschedule($id);
        $data = [
            'meetroom' => $getmeetroom,
            'ongoing' => $ongoing,
            'notif' => $notif
        ];
        // dd($data);
        return view('meetingroom/index', $data);
    }

    public function schedulemeeting($id_meeting = null)
    {
        $id = user_id();
        $getmeetroom = $this->meetingRoomModel->getmeetroom($id);
        $ongoing = $this->meetingRoomModel->ongoingschedule($id);
        $meetroom = $this->meetingRoomModel->meetroomto($id_meeting);
        $data = [
            'meetroom' => $getmeetroom,
            'ongoing' => $ongoing,
            'mt' => $meetroom
        ];
        // dd($data);
        return view('meetingroom/scheduleform',$data);
    }

    public function schedulemeetingof($id_meeting = null)
    {
        $id = user_id();
        $getmeetroom = $this->meetingRoomModel->getmeetroom($id);
        $ongoing = $this->meetingRoomModel->ongoingschedule($id);
        $meetroom = $this->meetingRoomModel->meetroomto($id_meeting);
        $data = [
            'meetroom' => $getmeetroom,
            'ongoing' => $ongoing,
            'mt' => $meetroom
        ];
        // dd($data);
        return view('meetingroom/scheduleformof',$data);
    }

    public function userschedule($id = null)
    {
        $id = user_id();
        $userschedule = $this->meetingRoomModel->userschedule($id);
        $data = [
            'userschedule' => $userschedule
        ];

        // dd($userschedule);
        return view('meetingroom/allschedule', $data);
    }

    public function notulen($idmeeting = null, $idofmeeting = null)
    {
        $id = user_id();
        
        $userschedule = $this->meetingRoomModel->authschedulenote($idmeeting,$idofmeeting);
        $participant = $this->meetingRoomModel->participant($idmeeting);
        $notulenbefore = $this->meetingRoomModel->notulenbefor($idofmeeting);
        $data = [
            'userschedule' => $userschedule,
            'participant' => $participant,
            'notulenbefore' => $notulenbefore
        ];
        // dd($data);
        return view('meetingroom/notulen', $data);
    }

    public function scheduledetail($idmeeting = null)
    {
        $id = user_id();
        $userschedule = $this->meetingRoomModel->authschedule($idmeeting);
        $participant = $this->meetingRoomModel->participant($idmeeting);
        $data = [
            'userschedule' => $userschedule,
            'participant' => $participant
        ];
        // dd($data);
        return view('meetingroom/scheduledetail', $data);
    }

    public function insertnotulen()
    {
        $id_meeting = $this->request->getVar('id_meeting');
        $notulen = $this->request->getVar('notulen');
        $id_notulen = $this->request->getVar('id_notulen');
        $status = $this->request->getVar('status');
        $data = [
            'notulen' => $notulen
        ];

        $schdlatatus = [
            'statuse' => $status
        ];
        // dd($data);
        $this->meetingRoomModel->update($id_meeting, $schdlatatus);
        $this->notulenModel->update($id_notulen, $data);
        return redirect()->to("allschedule");
    }

    public function addnotulis()
    {
        $id = $this->request->getVar('id');
        $id_meeting = $this->request->getVar('id_meeting');

        for ($i = 0; $i < sizeof($id); $i++) {
            $data = [
                'id' => $id[$i],
                'id_meeting' => $id_meeting,
                'satat' => '1'
            ];
            $this->notulenModel->insert($data);
        }
        return redirect()->to("meetingroom");
    }

    public function notulis($idmeeting = null)
    {

        $partinot = $this->meetingRoomModel->participantnotulis($idmeeting);
        $participant = $this->meetingRoomModel->participant($idmeeting);
        $data = [
            'participantnotulis' => $partinot,
            'participant' => $participant
        ];
        // dd($data);
        return view('meetingroom/notulis', $data);
    }

    public function scheduleadd()
    {
        $id = user_id();
        $date = $this->request->getVar('date');
        $start = $this->request->getVar('start');
        $end = $this->request->getVar('end');
        $topic = $this->request->getVar('topic');
        $vanue = $this->request->getVar('vanue');
        $tools = $this->request->getVar('perlengkapan');
        $ongoing = $this->request->getVar('continued');
        $data = [
            'id' => $id,
            'start_date' => $date,
            'start_time' => $start,
            'end_time' => $end,
            'topics_of_meeting' => $topic,
            'vanue' => $vanue,
            'tools' => $tools,
            'idofmeeting' => $ongoing
        ];
        
        $this->meetingRoomModel->insert($data);
        session()->setFlashdata('messager', 'Berhasil');
        return redirect()->to("meetingroom");
    }

    public function allschedule()
    {

        return view('meetingroom/allschedule');
    }

    public function participant($id_meeting)
    {


        $meetroom = $this->meetingRoomModel->meetroom($id_meeting);
        $it = $this->usersModel->userIT();
        $exim = $this->usersModel->userExim();
        $fat = $this->usersModel->userFAT();
        $gm = $this->usersModel->userGM();
        $hrga = $this->usersModel->userHRGA();
        $om = $this->usersModel->userOM();
        $marketing = $this->usersModel->userMarketing();
        $ppic = $this->usersModel->userPPIC();
        $produksi = $this->usersModel->userProduksi();
        $purch = $this->usersModel->userPurch();
        $qc = $this->usersModel->userQC();
        $teknik = $this->usersModel->userTeknik();
        $warehouse = $this->usersModel->userWarehouse();

        $data = [
            'meetroom' => $meetroom,
            'it' => $it,
            'exim' => $exim,
            'fat' => $fat,
            'gm' => $gm,
            'hrga' => $hrga,
            'om' => $om,
            'marketing' => $marketing,
            'ppic' => $ppic,
            'produksi' => $produksi,
            'purch' => $purch,
            'qc' => $qc,
            'teknik' => $teknik,
            'warehouse' => $warehouse,

        ];
        // dd($data);
        return view('meetingroom/participant', $data);
    }

    public function addparticipant()
    {
        $idparticipant = $this->request->getVar('participant');
        $id_meeting = $this->request->getVar('id_meeting');
        for ($i = 0; $i < sizeof($idparticipant); $i++) {
            $data = [
                'id' => $idparticipant[$i],
                'id_meeting' => $id_meeting
            ];
            $this->participantModel->insert($data);
        }
        // dd($data);
        session()->setFlashdata('messager', 'Success add participant');
        return redirect()->to("meetingroom");
        // dd($idparticipant);
    }

    public function attendance()
    {
        $id = user_id();
        $id_kehadiran = $this->request->getVar('idkehadiran');
        $attend = $this->request->getVar('attend');
        $ket = $this->request->getVar('ket');
        $data =[
            'attend' => $attend,
            'keterangan' => $ket
        ];
        // dd($data);
        $this->participantModel->update($id_kehadiran,$data);
        return redirect()->to("allschedule");
    }

    public function attend($id_meeting)
    {
        $id = user_id();
        $attendance = $this->meetingRoomModel->attendance($id_meeting,$id);
        $data = [
            'attendace' => $attendance
        ];
        // dd($data);
        return view('meetingroom/attend', $data);
    }

    public function approvalroom()
    {
        // $apprv = $this->request->getPost('apprv');
        $id_meeting = $this->request->getPost('id_meeting');
        for ($i = 0; $i < sizeof($id_meeting); $i++) {
            $data = [
                // 'id_meeting' => $id_meeting[$i],
                'apprv' => 1
            ];
            // dd($apprv);
            $this->meetingRoomModel->update($id_meeting[$i],$data);
        }
        return redirect()->to("hr/meetingroom");
    }

    public function notifschedule($id = null)
    {
        $id = user_id();
        $notif = $this->meetingRoomModel->notifschedule($id);
        $data = [
            'notif' => $notif
        ];
        // dd($data);
        $ter = ['6'];
        return view('templates/navbar', $ter);
    }
}
