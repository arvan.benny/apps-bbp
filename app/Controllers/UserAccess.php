<?php

namespace App\Controllers;
use App\Models\UserAccessModel;
use App\Models\AuthGroupsUsersModel;
use CodeIgniter\HTTP\Request;

class UserAccess extends BaseController
{
    protected $userModel;
    public function __construct()
    {
        $this->userModel = new UserAccessModel();
        $this->authGroupsUsersModel = new AuthGroupsUsersModel();
    }
    public function index()
    {
        return view('user/index');
    }

    public function profile($id = null)
    {
        $users = $this->userModel->getUser($id);
        $role = $this->userModel->getRole($id);
        $data = [
            'title' => 'BBP | User Detail',
            'h1' => 'User Detail',
            'users' => $users,
            'role' => $role
        ];
        return view('user/profile',$data);
    }

    public function addaccess($id = null)
    {
        $idaccess = $this->request->getPost('access');
        $user_id  = $this->request->getVar('iduser');
        for ($i=0; $i < sizeof($idaccess); $i++) 
        { 
           $data = [
            'group_id' => $idaccess[$i],
            'user_id' => $user_id
        ];
           $this->authGroupsUsersModel->insert($data);
        }
        // dd($data);
        return redirect()->to("admin");
    }
}
