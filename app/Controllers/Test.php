<?php

namespace App\Controllers;
use App\Models\UsersModel;
use App\Models\TestModel;

class Test extends BaseController
{
    protected $userModel;
    protected $testModel;
    public function __construct()
    {
        $this->userModel = new UsersModel();
        $this->testModel = new TestModel();
    }
    public function index()
    {
        $test = $this->testModel->allgroup();
        $data =[
            'test' => $test
        ];
        dd($data);
        // return view('user/index',$data);
    }
}
