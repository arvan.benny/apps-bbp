<?php

namespace App\Controllers;
use App\Models\OperatorModel;

class Operators extends BaseController
{
    protected $operatorModel;
    public function __construct()
    {
        $this->operatorModel = new OperatorModel();
    }
    public function index()
    {
        $cek = $this->operatorModel->all();
        $data = [
            'title' => 'Operator Engine Checker',
            'cek' => $cek
        ];
        // dd($cek);
        return view('operator/index', $data);
    }

    public function enginecheck($id_inkomop = null)
    {
        $id_inkomop = $this->request->getVar('id_inkomop');
        $list = $this->operatorModel->getlistindikator($id_inkomop);
        $detail = $this->operatorModel->getdetailindikator();
        $data =[
            'list' => $list,
            'detail' => $detail
        ];
        // dd($data);
        return view('operator/enginecheck',$data);
    }

    public function detail($id_listindikator = null)
    {
        $id_listindikator = $this->request->getVar('id_listindikator');
        $detail = $this->operatorModel->getdetailindikator($id_listindikator);
        $data = [
            'detail' => $detail
        ];
        // dd($detail);
        return view('operator/detail',$data);
    }
}
