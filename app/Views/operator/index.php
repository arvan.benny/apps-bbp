<?= $this->extend('templates/index'); ?>
<?= $this->section('page-content'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"><?= $title; ?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"><?= $title; ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row mb-2">
              <div class="col-md-12">
                <div class="card card-default">
                  <div class="card-header">
                  
                    <Form class="operator" action="<?= base_url('operator/enginecheck'); ?>" method="POST">
                    <div class="form-group">
                        <label>Select Indikator Items</label>
                        <select class="custom-select" name="id_inkomop">
                        <?php foreach ($cek as $incek): ?>
                          <option value="<?= $incek->id_inkomop; ?>"><?= $incek->inkomop ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </Form>
                  </div>
                </div>
              </div>
        </div>
    <!-- /.content -->
  </div>
  </div>
      <!-- /.container-fluid -->
    </section>

  <?= $this->endSection(); ?>
 