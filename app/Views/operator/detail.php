<?= $this->extend('templates/index'); ?>
<?= $this->section('page-content'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Persiapan penggunaan mesin</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">My Dasboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Pilih Indikator</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <form>
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- checkbox -->
                      <?php if ($detail): ?>
                        <?php foreach($detail as $detil): ?>
                      <div class="form-check">
                      <div class="custom-control custom-checkbox">
                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                        <label class="form-check-label" for="flexCheckDefault">
                          <?= $detil->detail_indikator; ?>
                        </label>
                      </div>
                      <br>
                      </div>
                      <?php endforeach; ?>
                      <?php endif; ?>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
    <!-- /.content -->
  </div>

  <?= $this->endSection(); ?>
 