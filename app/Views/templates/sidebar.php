<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href=<?= base_url('/'); ?> class="brand-link">
    <!-- <img src="<?= base_url(); ?>/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
    <span class="brand-text font-weight-light">Bahana Bhumipala Persada</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="<?= base_url(); ?>/dist/img/<?= user()->img; ?>" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block"><?= user()->fullname; ?></a>
      </div>
    </div>

    <!-- SidebarSearch Form -->
    <div class="form-inline">
      <div class="input-group" data-widget="sidebar-search">
        <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-sidebar">
            <i class="fas fa-search fa-fw"></i>
          </button>
        </div>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <li class="nav-header">GENERAL</li>
        <?php if (in_groups('admin')) : ?>
          <li class="nav-item">
            <a href=<?= base_url('admin'); ?> class="nav-link">
              <i class="nav-icon fas fa-user-cog"></i>
              <p>
                User Management
              </p>
            </a>
          <?php endif; ?>

          <!-- <li class="nav-item">
            <a href=<?= base_url('meetingroom'); ?> class="nav-link">
              <i class="fas fa-users"></i>
              <p>
                Meeting Room
              </p>
            </a>
          <li class="nav-item">
            <a href=<?= base_url('allschedule'); ?> class="nav-link">
              <i class="fas fa-calendar-alt"></i>
              <p>
                Meeting Schedules
              </p>
            </a> -->

            <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Module
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href=<?= base_url('premix'); ?> class="nav-link">
                  <i class="nav-icon fas fa-users"></i>
                  <p>
                    Premix
                  </p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Meetings
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href=<?= base_url('meetingroom'); ?> class="nav-link">
                  <i class="nav-icon fas fa-users"></i>
                  <p>
                    Meeting Room
                  </p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href=<?= base_url('allschedule'); ?> class="nav-link">
                  <i class="nav-icon fas fa-calendar-alt"></i>
                  <p>
                    Meeting Schedules
                  </p>
                </a>
              </li>
            </ul>
          </li>


          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Job Card
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/examples/invoice.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Job Card Teknik</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/examples/invoice.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Job Card IT</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-header">MODULES</li>
          <li class="nav-item">
            <?php if (in_groups('teknisi')) : ?>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Teknisi
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/examples/invoice.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Engine Checker</p>
                </a>
              </li>
            </ul>
          </li>
        <?php endif; ?>

        <?php if (in_groups('operator')) : ?>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Operator
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url('operator'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Engine Checker</p>
                </a>
              </li>
            </ul>
          </li>
        <?php endif; ?>

        <?php if (in_groups('owner')) : ?>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Owner
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url('operator'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Meetings Notulen</p>
                </a>
              </li>
            </ul>
          </li>
        <?php endif; ?>

        <?php if (in_groups('accounting')) : ?>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Accounting
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/examples/invoice.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Invoice</p>
                </a>
              </li>
            </ul>
          </li>
        <?php endif; ?>

        <?php if (in_groups('ppic')) : ?>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                PPIC
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/examples/invoice.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sales Order</p>
                </a>
              </li>
            </ul>
          </li>
        <?php endif; ?>

        <?php if (in_groups('hr')) : ?>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Human Resources
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/examples/invoice.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Recruitment</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url('hr/meetingroom'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Meeting Room</p>
                </a>
              </li>
            </ul>
          </li>
        <?php endif; ?>

        <?php if (in_groups('marketing')) : ?>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Marketing
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/examples/invoice.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Marketing</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/examples/invoice.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Marketing</p>
                </a>
              </li>
            </ul>
          </li>
        <?php endif; ?>

        <?php if (in_groups('produksi')) : ?>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Produksi
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/examples/invoice.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Product Order</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/examples/invoice.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>PREMIX</p>
                </a>
              </li>
            </ul>
          </li>
        <?php endif; ?>

        <?php if (in_groups('purchasing')) : ?>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
              Purchasing
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/examples/invoice.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Purchase Order</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/examples/invoice.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>PREMIX</p>
                </a>
              </li>
            </ul>
          </li>
        <?php endif; ?>

        <li class="nav-header">SETTINGS</li>
        <li class="nav-item">
          <a href=<?= base_url('user/profile/' . user()->id); ?> class="nav-link">
            <i class="fas fa-user"></i>
            <p>
              My Profile
            </p>
          </a>
        </li>

        <li class="nav-item">
          <a href=# class="nav-link">
            <i class="fas fa-user-edit"></i>
            <p>
              Profile Setting
            </p>
          </a>
        </li>

        <!-- <li class="nav-item">
            <a href=<?= base_url('logout'); ?> class="nav-link">
              <i class="fas fa-sign-out-alt"></i>
              <p>
                Logout
              </p>
            </a>
          </li> -->
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>