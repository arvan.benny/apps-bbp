<?= $this->extend('templates/index'); ?>
<?= $this->section('page-content'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Meeting Schedule last 3 day's</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">My Dasboard</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="card-body">
    <form action="<?= base_url('hr/approvalroom'); ?>" method="POST">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th style="width: 10px">No.</th>
            <th>Topics of Meeting</th>
            <th>PIC</th>
            <th>Date</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Tools</th>
            <th>Venue</th>
            <th>Approval</th>
            <th>Status</th>
            <th style="width: 40px">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i = 1; ?>
          <?php foreach ($meetroom as $mt) : ?>
            <tr>
              <th scope="row"><?= $i++ ?></th>

              <td><?= $mt->topics_of_meeting; ?></td>
              <td><?= $mt->fullname; ?></td>
              <td><?= $mt->start_date; ?></td>
              <td><?= $mt->start_time; ?></td>
              <td><?= $mt->end_time; ?></td>
              <td><?= $mt->tools; ?></td>
              <td><?= $mt->vanue; ?></td>
              <?php if ($mt->apprv == 1) {
                $status = "Approved";
                $badges = "success";
              } else {
                $status = "No Approved";
                $badges = "danger";
              } ?>
              <td><span class="badge badge-<?= $badges; ?> text-uppercase">
                <?= $status ?></td>
              <?php if ($mt->statuse == 0) {
                $sta = "-";
                $badge = "";
              } elseif ($mt->statuse == 1) {
                $sta = "Done";
                $badge = "success";
              } else {
                $sta = "On Going";
                $badge = "warning";
              } ?> 
              <td><span class="badge badge-<?= $badge; ?> text-uppercase">
                  <?= $sta ?></td>
              </span>
              <td>
                <input type="checkbox" value="<?= $mt->id_meeting; ?>" name="id_meeting[]">
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
      <br><button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
  <!-- /.content -->
</div>

<?= $this->endSection(); ?>