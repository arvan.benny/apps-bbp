<?= $this->extend('templates/index'); ?>
<?= $this->section('page-content'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Schedule Meeting Room</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Schedule Meeting Room</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  
  <div class="card-body">
    <a href="schedulemeeting" class="btn btn-success">Add Schedule</a>
    <br>
    <form action="<?= base_url('admin/detail'); ?>" method="POST">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th style="width: 10px">No.</th>
            <th>Topics of Meeting</th>
            <th>PIC</th>
            <th>Date</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Venue</th>
            <th>Approval</th>
            <th>Status</th>
            <th style="width: 40px">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i = 1; ?>
          <?php foreach ($meetroom as $mt) : ?>
            <tr>
              <th scope="row"><?= $i++ ?></th>

              <td><?= $mt->topics_of_meeting; ?></td>
              <td><?= $mt->fullname; ?></td>
              <td><?= $mt->start_date; ?></td>
              <td><?= $mt->start_time; ?></td>
              <td><?= $mt->end_time; ?></td>
              <td><?= $mt->vanue; ?></td>
              <?php if ($mt->apprv == 1) {
                $status = "Approved";
              } else {
                $status = "-";
              } ?>
              <td><?= $status ?></td>
              <?php if ($mt->statuse == 0) {
                $sta = "-";
                $badge = "";
              } elseif ($mt->statuse == 1) {
                $sta = "Done";
                $badge = "success";
              } else {
                $sta = "On Going";
                $badge = "warning";
              } ?>
              <td><span class="badge badge-<?= $badge; ?> text-uppercase">
                  <?= $sta ?></td>
              </span>
              <td>
                <div class="btn-group">
                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    act
                  </button>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="<?= base_url('participant/' . $mt->id_meeting); ?>">Participant</a>
                    <a class="dropdown-item" href="<?= base_url('notulis/' . $mt->id_meeting); ?>">Notulis</a>
                    <a class="dropdown-item" href="<?= base_url('schedule/scheduledetail/' . $mt->id_meeting); ?>">Detail</a>
                    <?php if ($mt->statuse == 2) {
                      $hide = "";
                    } else {
                      $hide = "hidden";
                    } ?>
                    <a class="dropdown-item" href="<?= base_url('schedulemeetingof/' . $mt->id_meeting); ?>" <?= $hide; ?>>Continued</a>
                  </div>
                </div>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </form>
  </div>
  <!-- /.content -->
</div>

<?= $this->endSection(); ?>