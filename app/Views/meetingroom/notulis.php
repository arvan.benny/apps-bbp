<?= $this->extend('templates/index'); ?>
<?= $this->section('page-content'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <?php foreach($participantnotulis as $pn): ?>
          <h1 class="m-0">Notulis - <?= $pn->topics_of_meeting; ?> </h1>
          <?php endforeach ?>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Notulis</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <!-- Table row -->

  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-5">

          <div class="card card-primary card-outline">
            <div class="card-body box-profile">
              <form action="<?= base_url('/addnotulis'); ?>" method="POST">
              <input type="text" value="<?= $pn->id_meeting; ?>" name="id_meeting" hidden>
                <div class="row">
                  <div class="col-12 table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>No.</th>
                          <th>Name</th>
                          <th>Dept</th>
                          <th>Notulis</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($participant as $parti) : ?>
                          <tr>
                            <td scope="row"><?= $i++ ?></td>
                            <td><?= $parti->fullname; ?></td>
                            <td><?= $parti->dept_name; ?></td>
                            <td><input type="checkbox" value="<?= $parti->partid; ?>" name="id[]"></td>
                          </tr>
                        <?php endforeach ?>
                      </tbody>
                    </table>
                  </div>
                  <!-- /.col -->
                </div>
                <button type="submit" class="btn btn-info btn-block">Submit</button>
              </form>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /.row -->
  <!-- /.content -->
</div>

<?= $this->endSection(); ?>