<?= $this->extend('templates/index'); ?>
<?= $this->section('page-content'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">All Schedule Meeting</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">My Dasboard</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="card-body">
    <form action="<?= base_url('admin/detail'); ?>" method="POST">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th style="width: 10px">No.</th>
            <th>Topics of Meeting</th>
            <th>PIC</th>
            <th>Date</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Venue</th>
            <th hidden>Id of Meeting</th>

            <th style="width: 40px">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $i = 1; ?>
          <?php foreach ($userschedule as $schedule) : ?>
            <tr>
              <th scope="row"><?= $i++ ?></th>

              <td><?= $schedule->topics_of_meeting; ?></td>
              <td><?= $schedule->nmfull; ?></td>
              <td><?= $schedule->start_date; ?></td>
              <td><?= $schedule->start_time; ?></td>
              <td><?= $schedule->end_time; ?></td>
              <td><?= $schedule->vanue; ?></td>
              <td hidden><?= $schedule->idofmeeting; ?></td>

              <td>
                <div class="btn-group">
                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Action
                  </button>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="<?= base_url('attend/'. $schedule->idmeeting) ?>">Attend</a>
                    <a class="dropdown-item" href="<?= base_url('schedule/scheduledetail/' . $schedule->idmeeting); ?>">Detail</a>
                    <?php if ($schedule->iduser == user_id()) {
                      $notulen = "";
                    } else {
                      $notulen = "hidden";
                    } ?>
                    <?php if (is_null($schedule->idofmeeting)) {
                      $idof = $schedule->idmeeting;
                    } else {
                      $idof = $schedule->idofmeeting;
                    } ?>
                    <a class="dropdown-item" href="<?= base_url('notulen/' . $schedule->idmeeting . '/'. $schedule->idofmeeting); ?>" <?= $notulen; ?>>Notulen</a>
                  </div>
                </div>
              </td>
            </tr>
            <!-- Modal -->
            <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form action="" method="POST">
                      <!-- Profile Image -->
                      <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                          <input type="text" value="<?= $schedule->idmeeting; ?>" name="idmeeting">
                          <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                              <b>Hadir</b> <a class="float-right"><input type="checkbox" value="1"></a>
                            </li>
                            <li class="list-group-item">
                              <b>Tidak Hadir</b> <a class="float-right"><input type="checkbox" value="0"></a>
                            </li>
                            <li class="list-group-item">
                              <b>Keterangan Tidak Hadir</b> <a class="float-right"><input type="text" value="" name="ket" class="form-control"></a>
                            </li>
                          </ul>

                          <a href="#" class="btn btn-primary btn-block"><b>Submit</b></a>
                        </div>
                        <!-- /.card-body -->
                      </div>
                    </form>
                    <!-- /.card -->
                  </div>
                  <!-- <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Understood</button>
        </div> -->
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </tbody>
      </table>
    </form>
  </div>

  <!-- /.content -->
</div>

<script>
  $()
</script>

<?= $this->endSection(); ?>