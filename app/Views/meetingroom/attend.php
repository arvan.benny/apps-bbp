<?= $this->extend('templates/index'); ?>
<?= $this->section('page-content'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Attendance of Meeting</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">My Dasboard</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <form action="<?= base_url('attendance/') ?>" method="POST">
                        <!-- Profile Image -->

                        <div class="card card-primary card-outline">
                            <div class="card-body box-profile">
                                <?php foreach($attendace as $attend): ?>
                                    <input type="text" value="<?= $attend->id_kehadiran; ?>" name="idkehadiran" hidden>
                                <input type="text" value="<?= $attend->id_meeting; ?>" name="idmeeting" hidden>
                                <?php endforeach; ?>
                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b>Hadir</b> <a class="float-right"><input type="checkbox" value="1" name="attend"></a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Tidak Hadir</b> <a class="float-right"><input type="checkbox" value="0" name="attend"></a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Keterangan Tidak Hadir</b> <a class="float-right"><input type="text" value="" name="ket" class="form-control"></a>
                                    </li>
                                </ul>
                                <button type="submit" class="btn btn-primary btn-block"><b>Submit</b></a>
                            </div>
                            <!-- /.card-body -->
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- /.content -->
</div>

<?= $this->endSection(); ?>