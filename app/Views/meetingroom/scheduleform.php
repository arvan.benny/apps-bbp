<?= $this->extend('templates/index'); ?>
<?= $this->section('page-content'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Request Meeting Schedule</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Meeting Room</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <section class="content">
    <div class="row">
      <div class="col-md-6">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Meeting Schedule</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
            <form method="POST" action="<?= base_url('meetingroom/scheduleadd'); ?>">
            <div class="form-group">
              <label for="tgl">Schedule Date</label>
              <input type="date" id="inputName" class="form-control" name="date">
            </div>

            <div class="form-group">
              <label for="tgl">Start Time</label>
              <input type="time" id="inputName" class="form-control" name="start">
            </div>

            <div class="form-group">
              <label for="tgl">End Time</label>
              <input type="time" id="inputName" class="form-control" name="end">
            </div>

            <div class="form-group">
              <label for="inputDescription">Topics of Meeting</label>
              <textarea id="inputDescription" class="form-control" rows="4" name="topic"></textarea>
            </div>
            <!-- <div class="form-group">
              <label for="com">Continuation of Meeting</label>
              <select id="vanue" class="form-control custom-select" name="continued">
                <option selected disabled>Select one</option>
                <?php foreach($ongoing as $ongo): ?>
                <option value="<?= $ongo->id_meeting; ?>"><?= $ongo->topics_of_meeting; ?> | <?= $ongo->start_date; ?></option>
                <?php endforeach; ?>
              </select>
            </div> -->
            <div class="form-group">
              <label for="vanue">Venue</label>
              <select id="vanue" class="form-control custom-select" name="vanue">
                <option selected disabled>Select one</option>
                <option value="Ruang Meeting Produksi">Ruang Meeting Produksi</option>
                <option value="Ruang Meeting Depan">Ruang Meeting Depan</option>
                <option value="Parkiran">Ruang Meeting Parkiran</option>
              </select>
            </div>
            <div class="form-group">
              <label for="perlengkapan">Meeting Tools</label>
              <input type="text" id="perlengkapan" class="form-control" name="perlengkapan">
            </div>
            <br><button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
    <!-- <div class="row">
      <div class="col-12">
        <a href="<?= base_url('/meetingroom'); ?>" class="btn btn-secondary">Cancel</a>
        <a href="#" class="btn btn-success">Submit</a>
      </div> -->
    </div>
  </section>
  <!-- Main content -->

  <!-- /.content -->
</div>

<?= $this->endSection(); ?>