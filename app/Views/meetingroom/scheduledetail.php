<?= $this->extend('templates/index'); ?>
<?= $this->section('page-content'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Schedule Detail</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Notulen</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="invoice p-3 mb-3">
        <!-- title row -->
        <div class="row">
            <div class="col-12">
                <h4>
                    <i class=""></i> Minute Of Meeting.
                    <!-- <small class="float-right">Date: 2/10/2014</small> -->
                </h4>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                <address>
                    <?php foreach ($userschedule as $us) : ?>
                        <hr>
                        <strong>Date : &emsp;</strong><?= $us->start_date; ?><br>
                        <hr>
                        <strong>Vanue : &emsp;</strong><?= $us->vanue; ?><br>
                        <hr>
                        <strong>Topics : &emsp;</strong><?= $us->topics_of_meeting; ?><br>
                    <?php endforeach ?>
                </address>
            </div>

            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <address>
                    <?php foreach ($userschedule as $us) : ?>
                        <hr>
                        <strong>Time : &emsp;</strong><?= $us->start_time; ?> - <?= $us->end_time; ?><br>
                        <hr>
                        <strong>Author : &emsp;</strong><?= $us->nmfull; ?><br>
                        <hr>
                    <?php endforeach ?>
                </address>
            </div>
            <!-- /.col -->

        </div>
        <!-- /.row -->



        <!-- Table row -->
        <div class="row">
            <div class="col-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Name</th>
                            <th>Dept</th>
                            <th>Sign</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($participant as $parti) : ?>
                            <tr>
                                <td scope="row"><?= $i++ ?></td>
                                <td><?= $parti->fullname; ?></td>
                                <td><?= $parti->dept_name; ?></td>
                                <?php if ($parti->attend == null) {
                                    $attendance = "-";
                                } elseif ($parti->attend == 0) {
                                    $attendance = "Tidak Hadir";
                                } elseif ($parti->attend == 1){
                                    $attendance = "Hadir";
                                }?>
                                <td><?= $attendance; ?></td>
                                <td><?= $parti->keterangan; ?></td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <form action="<?= base_url('/addnotulen') ?>" method="POST"><br>
            <u>
                <h3 class="m-0">Notulen</h3>
            </u>
            <?php foreach ($userschedule as $us) : ?>
                <div class="card-body">
                    <?= $us->notulen; ?>
                </div>
            <?php endforeach ?>
        </form>

        <!-- this row will not appear when printing -->
    </div>
    <!-- /.content -->
</div>

<?= $this->endSection(); ?>