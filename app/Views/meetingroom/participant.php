<?= $this->extend('templates/index'); ?>
<?= $this->section('page-content'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <?php foreach ($meetroom as $mr) : ?>
            <h1 class="m-0">Add Participant for <?= $mr->topics_of_meeting; ?></h1>
            
          <?php endforeach ?>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">My Dasboard</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-6">
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Participant</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <form action="<?= base_url('/addparticipant'); ?>" method="POST">
          <input type="text" value="<?= $mr->id_meeting; ?>" name="id_meeting" hidden>
            <div id="accordion">
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-1">
                    <a class="btn btn-info collapsed btn-sm" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                      General Manager
                    </a>
                  </h5>
                </div>

                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="card-body">
                    <div class="form-check">
                      <?php foreach($gm as $gmr): ?>
                      <input class="form-check-input" type="checkbox" id="flexCheckDefault" value="<?= $gmr->id; ?>" name="participant[]">
                      <label class="form-check-label" for="flexCheckDefault">
                        <?= $gmr->fullname; ?>
                      </label>
                      <?php endforeach ?>
                    </div>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="headingTwo">
                  <h5 class="mb-0">
                    <a class="btn btn-info collapsed btn-sm" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      Manager Operasional
                    </a>
                  </h5>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                  <div class="card-body">
                    <?php foreach($om as $omr): ?>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="<?= $omr->id; ?>" name="participant[]">
                      <label class="form-check-label" for="flexCheckDefault">
                        <?= $omr->fullname; ?>
                      </label>
                    </div>
                    <?php endforeach ?>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="headingThree">
                  <h5 class="mb-0">
                    <a class="btn btn-info collapsed btn-sm" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      HRGA
                    </a>
                  </h5>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                  <div class="card-body">
                    <?php foreach($hrga as $hr): ?>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="flexCheckDefault" value="<?= $hr->id; ?>" name="participant[]">
                      <label class="form-check-label" for="flexCheckDefault">
                        <?= $hr->fullname; ?>
                      </label>
                    </div>
                    <?php endforeach ?>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="headingIt">
                  <h5 class="mb-0">
                    <a class="btn btn-info collapsed btn-sm" data-toggle="collapse" data-target="#collapseIt" aria-expanded="false" aria-controls="collapseThree">
                      IT
                    </a>
                  </h5>
                </div>
                <div id="collapseIt" class="collapse" aria-labelledby="headingIt" data-parent="#accordion">
                  <div class="card-body">
                    <?php foreach($it as $ti): ?>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="flexCheckDefault" value="<?= $ti->id; ?>" name="participant[]">
                      <label class="form-check-label" for="flexCheckDefault">
                        <?= $ti->fullname; ?>
                      </label>
                    </div>
                    <?php endforeach ?>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="headingFour">
                  <h5 class="mb-0">
                    <a class="btn btn-info collapsed btn-sm" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                      EXIM
                    </a>
                  </h5>
                </div>

                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                  <div class="card-body">
                    <?php foreach($exim as $ei): ?>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="flexCheckDefault" value="<?= $ei->id; ?>" name="participant[]">
                      <label class="form-check-label" for="flexCheckDefault">
                        <?= $ei->fullname; ?>
                      </label>
                    </div>
                    <?php endforeach ?>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="headingFive">
                  <h5 class="mb-0">
                    <a class="btn btn-info collapsed btn-sm" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
                      FAT
                    </a>
                  </h5>
                </div>

                <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                  <div class="card-body">
                  <?php foreach($fat as $fa): ?>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="flexCheckDefault" value="<?= $fa->id; ?>" name="participant[]">
                      <label class="form-check-label" for="flexCheckDefault">
                        <?= $fa->fullname; ?>
                      </label>
                    </div>
                    <?php endforeach ?>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="headingSix">
                  <h5 class="mb-0">
                    <a class="btn btn-info collapsed btn-sm" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseThree">
                      Marketing
                    </a>
                  </h5>
                </div>

                <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                  <div class="card-body">
                  <?php foreach($marketing as $market): ?>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="flexCheckDefault" value="<?= $market->id; ?>" name="participant[]">
                      <label class="form-check-label" for="flexCheckDefault">
                        <?= $market->fullname; ?>
                      </label>
                    </div>
                    <?php endforeach ?>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="headingSeven">
                  <h5 class="mb-0">
                    <a class="btn btn-info collapsed btn-sm" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseThree">
                      PPIC
                    </a>
                  </h5>
                </div>

                <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                  <div class="card-body">
                  <?php foreach($ppic as $pic): ?>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="flexCheckDefault" value="<?= $pic->id; ?>" name="participant[]">
                      <label class="form-check-label" for="flexCheckDefault">
                        <?= $pic->fullname; ?>
                      </label>
                    </div>
                    <?php endforeach ?>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="headingEight">
                  <h5 class="mb-0">
                    <a class="btn btn-info collapsed btn-sm" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                      Produksi
                    </a>
                  </h5>
                </div>

                <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                  <div class="card-body">
                  <?php foreach($produksi as $produk): ?>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="flexCheckDefault" value="<?= $produk->id; ?>" name="participant[]">
                      <label class="form-check-label" for="flexCheckDefault">
                        <?= $produk->fullname; ?>
                      </label>
                    </div>
                    <?php endforeach ?>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="headingNine">
                  <h5 class="mb-0">
                    <a class="btn btn-info collapsed btn-sm" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseEight">
                      Purchasing
                    </a>
                  </h5>
                </div>

                <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                  <div class="card-body">
                  <?php foreach($purch as $purc): ?>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="flexCheckDefault" value="<?= $purc->id; ?>" name="participant[]">
                      <label class="form-check-label" for="flexCheckDefault">
                        <?= $purc->fullname; ?>
                      </label>
                    </div>
                    <?php endforeach ?>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="headingTen">
                  <h5 class="mb-0">
                    <a class="btn btn-info collapsed btn-sm" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                      Quality Control
                    </a>
                  </h5>
                </div>

                <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                  <div class="card-body">
                  <?php foreach($qc as $qco): ?>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="flexCheckDefault" value="<?= $qco->id; ?>" name="participant[]">
                      <label class="form-check-label" for="flexCheckDefault">
                        <?= $qco->fullname; ?>
                      </label>
                    </div>
                    <?php endforeach ?>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="headingEleven">
                  <h5 class="mb-0">
                    <a class="btn btn-info collapsed btn-sm" data-toggle="collapse" data-target="#collapseleven" aria-expanded="false" aria-controls="collapseleven">
                      Teknik
                    </a>
                  </h5>
                </div>

                <div id="collapseleven" class="collapse" aria-labelledby="headingleven" data-parent="#accordion">
                  <div class="card-body">
                  <?php foreach($teknik as $tekni): ?>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="flexCheckDefault" value="<?= $tekni->id; ?>" name="participant[]">
                      <label class="form-check-label" for="flexCheckDefault">
                        <?= $tekni->fullname; ?>
                      </label>
                    </div>
                    <?php endforeach ?>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="headingTwlv">
                  <h5 class="mb-0">
                    <a class="btn btn-info collapsed btn-sm" data-toggle="collapse" data-target="#collapseTwlv" aria-expanded="false" aria-controls="collapseTwlv">
                    Warehouse
                    </a>
                  </h5>
                </div>

                <div id="collapseTwlv" class="collapse" aria-labelledby="headingTwlv" data-parent="#accordion">
                  <div class="card-body">
                  <?php foreach($warehouse as $whs): ?>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="flexCheckDefault" value="<?= $whs->id; ?>" name="participant[]">
                      <label class="form-check-label" for="flexCheckDefault">
                        <?= $whs->fullname; ?>
                      </label>
                    </div>
                    <?php endforeach ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-body">
            <br><button type="submit" class="btn btn-primary">Submit</button>
          </div>
          </form>
          
        </div>
        <!-- /.card-body -->
      </div>
  </section>

  <!-- /.content -->
</div>
</div>



<?= $this->endSection(); ?>