<?= $this->extend('templates/index'); ?>
<?= $this->section('page-content'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Notulen</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Notulen</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="invoice p-3 mb-3">
        <!-- title row -->
        <div class="row">
            <div class="col-12">
                <h4>
                    <i class=""></i> Minute Of Meeting.
                    <!-- <small class="float-right">Date: 2/10/2014</small> -->
                </h4>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                <address>
                    <?php foreach ($userschedule as $us) : ?>
                        <hr>
                        <strong>Date : &emsp;</strong><?= $us->start_date; ?><br>
                        <hr>
                        <strong>Vanue : &emsp;</strong><?= $us->vanue; ?><br>
                        <hr>
                        <strong>Topics : &emsp;</strong><?= $us->topics_of_meeting; ?><br>
                    <?php endforeach ?>
                </address>
            </div>

            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <address>
                    <?php foreach ($userschedule as $us) : ?>
                        <hr>
                        <strong>Time : &emsp;</strong><?= $us->start_time; ?> - <?= $us->end_time; ?><br>
                        <hr>
                        <strong>Author : &emsp;</strong><?= $us->nmfull; ?><br>
                        <hr>
                    <?php endforeach ?>
                </address>
            </div>
            <!-- /.col -->

        </div>
        <!-- /.row -->



        <!-- Table row -->
        <div class="row">
            <div class="col-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Name</th>
                            <th>Dept</th>
                            <th>Sign</th>
                            <th>Keterangan Sign</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($participant as $parti) : ?>
                            <tr>
                                <td scope="row"><?= $i++ ?></td>
                                <td><?= $parti->fullname; ?></td>
                                <td><?= $parti->dept_name; ?></td>
                                <?php if ($parti->attend == null) {
                                    $attendance = "-";
                                } elseif ($parti->attend == 0) {
                                    $attendance = "Tidak Hadir";
                                } elseif ($parti->attend == 1){
                                    $attendance = "Hadir";
                                }?>
                                <td><?= $attendance; ?></td>
                                <td><?= $parti->keterangan; ?></td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <form action="<?= base_url('/addnotulen') ?>" method="POST">
            <input type="text" value="<?= $parti->id_meeting; ?>" hidden name="id_meeting">
            <?php foreach ($userschedule as $us) : ?>
                <input type="text" value="<?= $us->id_notulen; ?>" hidden name="id_notulen">
            <?php endforeach ?>
            <?php foreach ($userschedule as $usersch) : ?>
                <!-- <input type="text" value="<?= $usersch->idofmeeting; ?>" name="id_notulen"> -->
            <?php endforeach; ?>
            <div class="card-body">
                <textarea id="summernote" name="notulen">
                <?php foreach ($userschedule as $usersch) : ?>
                    <?php if ($usersch->notulen == null) {
                        $note = '<table class="table table-bordered"><tbody><tr><td><b>No.</b></td><td><b>Subject</b></td><td><b>Discussion/Action plan</b></td><td><b>Due Date</b></td><td><b>PIC</b></td><td><b>Status</b></td></tr><tr><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td></tr><tr><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td></tr><tr><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td></tr><tr><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td></tr><tr><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td></tr><tr><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td></tr></tbody></table>';
                    } else {
                        $note = $usersch->notulen;
                    } ?>
                <?php endforeach; ?>
                <?php if ($notulenbefore == null) {
                    $notulens = $note;
                } else {
                    $notulens = $notulenbefore->notulen;
                } ?>
                <?= $notulens; ?>
                </textarea>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="status" id="exampleRadios1" value="1" checked>
                    <label class="form-check-label" for="exampleRadios1">
                        Done
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="status" id="exampleRadios2" value="2">
                    <label class="form-check-label" for="exampleRadios2">
                        On Going
                    </label>
                </div>
                <button type="submit" class="btn btn-success float-right">Submit
                </button>
            </div>
        </form>

        <!-- this row will not appear when printing -->
    </div>
    <!-- /.content -->
</div>

<?= $this->endSection(); ?>