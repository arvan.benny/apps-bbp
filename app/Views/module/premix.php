<?= $this->extend('templates/index'); ?>
<?= $this->section('page-content'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Pemix</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Premix</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="card-body" id="cardbody">
    <div class="row">
      <div class="col-sm-0">
        <label for="">Select Date</label>
        <input type="text" id="date-premix" class="form-control">
      </div>
    </div>
    <br>
    <button type="button" class="btn btn-info" id="btn-premix">Get Data</button>
    <hr>
    <table id="premix-tbl1" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th style="width: 10px">No.</th>
          <th>Item Code</th>
          <th>Nomor PO</th>
          <th>Product Name</th>
          <th>Description</th>
          <th>Warehouse</th>
          <th>OpenQty</th>
          <th>Price</th>
          <th>TotalSumSy</th>
        </tr>
      </thead>
      <!-- <tbody>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </tbody> -->
    </table>
    <hr>
    <label>Buyer Selection</label>
    <select class="form-control select2bs4" style="width: 100%;" id="bp-select">
      <option selected="selected">--Select Buyer--</option>
    </select>

    <!-- /.content -->
  </div>
</div>

<?= $this->endSection(); ?>