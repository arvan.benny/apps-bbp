<?= $this->extend('templates/index'); ?>
<?= $this->section('page-content'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">My Dasboard</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">My Dasboard</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="card-body">
    <a href="schedulemeeting" class="btn btn-success">Add Schedule</a>
    <br>
    <form action="<?= base_url('admin/detail'); ?>" method="POST">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th style="width: 10px">No.</th>
            <th>Topics of Meeting</th>
          </tr>
        </thead>
        <tbody>
          <?php $i = 1; ?>

            <tr>
              <th scope="row"><?= $i++ ?></th>

              <td></td>
            </tr>
        </tbody>
      </table>
    </form>
  </div>
  <!-- /.content -->
</div>

<?= $this->endSection(); ?>