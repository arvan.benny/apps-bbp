<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (is_file(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'User::index');
$routes->get('/user', 'User::index');
$routes->get('/user/profile', 'User::profile');
$routes->get('/user/profile/(:num)', 'User::profile/$1');
$routes->get('/admin', 'Admin::index', ['filter' => 'role:admin']);
$routes->get('/admin/index', 'Admin::index', ['filter' => 'role:admin']);
$routes->get('/admin/(:num)', 'Admin::detail/$1', ['filter' => 'role:admin']);

$routes->post('/access', 'UserAccess::addaccess');

$routes->get('/hr', 'Hr::detail', ['filter' => 'role:hr']);
$routes->get('hr/meetingroom', 'Hr::meetingroom', ['filter' => 'role:hr']);
$routes->post('hr/approvalroom', 'MeetingRoom::approvalroom', ['filter' => 'role:hr']);



$routes->get('/teknisi', 'Teknisi::index', ['filter' => 'role:teknisi']);
$routes->get('/accounting', 'Accounting::index', ['filter' => 'role:accounting']);
$routes->get('/ppic', 'Ppic::index', ['filter' => 'role:accounting']);
$routes->get('/operator', 'Operators::index', ['filter' => 'role:operator']);
$routes->get('/operator/enginecheck', 'Operators::enginecheck', ['filter' => 'role:operator']);
$routes->post('/operator/enginecheck', 'Operators::enginecheck', ['filter' => 'role:operator']);
$routes->post('/operator/detailcheck', 'Operators::detailcheck', ['filter' => 'role:operator']);
$routes->post('/operator/detail', 'Operators::detail', ['filter' => 'role:operator']);

$routes->get('/meetingroom', 'MeetingRoom::index');
$routes->get('/schedulemeeting', 'MeetingRoom::schedulemeeting');
$routes->get('/schedulemeetingof/(:num)', 'MeetingRoom::schedulemeetingof/$1');
$routes->get('/allschedule', 'MeetingRoom::userschedule');
$routes->get('/participant/(:num)', 'MeetingRoom::participant/$1');
$routes->post('/meetingroom/scheduleadd', 'MeetingRoom::scheduleadd');
$routes->post('/addparticipant', 'MeetingRoom::addparticipant');
$routes->get('notulen/(:num)/(:num)', 'MeetingRoom::notulen/$1/$2');
$routes->get('notulen/(:num)', 'MeetingRoom::notulen/$1');
$routes->post('/addnotulen', 'MeetingRoom::insertnotulen');

$routes->get('schedule/scheduledetail/(:num)', 'MeetingRoom::scheduledetail/$1');
$routes->get('notulis/(:num)', 'MeetingRoom::notulis/$1');
$routes->post('addnotulis/', 'MeetingRoom::addnotulis');
$routes->get('attend/(:num)', 'MeetingRoom::attend/$1');
$routes->get('test', 'Test::index');
$routes->post('attendance/', 'MeetingRoom::attendance');

$routes->get('premix/','Module::premix');
$routes->get('bp/','Module::businesspartner');


/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
