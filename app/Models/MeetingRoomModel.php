<?php

namespace App\Models;

use CodeIgniter\Model;

class MeetingRoomModel extends Model
{
    protected $table = 'meeting_schedule';
    protected $primaryKey = 'id_meeting';
    protected $useTimestamps = true;
    protected $allowedFields = [
        'id_meeting',
        'id',
        'start_date',
        'end_date',
        'vanue',
        'pic',
        'notulis',
        'start_time',
        'end_time',
        'topics_of_meeting',
        'apprv',
        'statuse',
        'tools',
        'idofmeeting'

    ];
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';

    public function getmeetroom($id)
    {
        $builder = $this->db->table('meeting_schedule');
        $builder->select('id_meeting, topics_of_meeting, start_date, start_time, end_time, vanue, apprv, statuse, fullname, tools');
        $builder->join('users', 'users.id = meeting_schedule.id');
        $builder->where('meeting_schedule.id', $id);
        $query = $builder->get();
        return $query->getResult();
    }

    public function meetroom($id_meeting)
    {
        $builder = $this->db->table('meeting_schedule');
        $builder->select('id_meeting, topics_of_meeting, start_date, start_time, end_time, vanue, apprv, statuse, fullname, tools');
        $builder->join('users', 'users.id = meeting_schedule.id');
        $builder->where('meeting_schedule.id_meeting', $id_meeting);
        $query = $builder->get();
        return $query->getResult();
    }

    public function meetroomto($id_meeting)
    {
        $builder = $this->db->table('meeting_schedule');
        $builder->select('id_meeting, topics_of_meeting, start_date, start_time, end_time, vanue, apprv, statuse, fullname');
        $builder->join('users', 'users.id = meeting_schedule.id');
        $builder->where('meeting_schedule.id_meeting', $id_meeting);
        $query = $builder->get();
        return $query->getRow();
    }

    public function userschedule($id)
    {
        $builder = $this->db->table('meeting_schedule T0');
        $builder->select('T0.id_meeting as idmeeting, T0.idofmeeting, T0.topics_of_meeting, T0.start_date, T0.start_time, T0.end_time, T0.vanue, T0.apprv, statuse, idofmeeting, T3.fullname as nmfull, T2.fullname as namefull, T4.satat, T4.id as iduser, T4.id_notulen, T4.notulen');
        $builder->join('meeting_participant T1', 'T0.id_meeting = T1.id_meeting');
        $builder->join('users T2', 'T1.id = T2.id');
        $builder->join('users T3', 'T0.id = T3.id');
        $builder->join('meeting_notulen T4', 'T0.id_meeting = T4.id_meeting');
        $builder->where('T1.id', $id);
        $builder->where('T0.apprv', 1);
        $query = $builder->get();
        return $query->getResult();
    }

    public function participant($idmeeting)
    {
        $builder = $this->db->table('meeting_schedule T0');
        $builder->select('T0.id_meeting, T2.fullname, T3.dept_name, T1.attend, T1.keterangan, T0.topics_of_meeting, T1.id as partid');
        $builder->join('meeting_participant T1', 'T0.id_meeting = T1.id_meeting');
        $builder->join('users T2', 'T1.id = T2.id');
        $builder->join('dept T3', 'T2.id_dept = T3.id_dept');
        $builder->where('T0.id_meeting', $idmeeting);
        $query = $builder->get();
        return $query->getResult();
    }

    public function participantnotulis($idmeeting)
    {
        $builder = $this->db->table('meeting_schedule T0');
        $builder->select('T0.id_meeting, T0.topics_of_meeting ');
        $builder->where('T0.id_meeting', $idmeeting);
        $query = $builder->get();
        return $query->getResult();
    }

    public function authschedule($idmeeting)
    {
        $builder = $this->db->table('meeting_schedule T0');
        $builder->select('T0.id_meeting, T0.idofmeeting, T0.topics_of_meeting, T0.start_date, T0.start_time, T0.end_time, T0.vanue, T0.apprv, T0.statuse, T1.fullname as nmfull, T2.id_notulen, T2.notulen ');
        $builder->join('users T1', 'T0.id = T1.id');
        $builder->join('meeting_notulen T2', 'T0.id_meeting= T2.id_meeting');
        $builder->where('T0.id_meeting', $idmeeting);
        $query = $builder->get();
        return $query->getResult();
    }

    public function authschedulenote($idmeeting, $idofmeeting)
    {
        if ($idofmeeting == null) {
            $builder = $this->db->table('meeting_schedule T0');
            $builder->select('T0.id_meeting, T0.idofmeeting, T0.topics_of_meeting, T0.start_date, T0.start_time, T0.end_time, T0.vanue, T0.apprv, T0.statuse, T1.fullname as nmfull, T2.id_notulen, T2.notulen ');
            $builder->join('users T1', 'T0.id = T1.id');
            $builder->join('meeting_notulen T2', 'T0.id_meeting= T2.id_meeting');
            $builder->where('T0.id_meeting', $idmeeting);
            $query = $builder->get();
            return $query->getResult();
        } else {
            $group = [$idmeeting, $idofmeeting];
            $builder = $this->db->table('meeting_schedule T0');
            $builder->select('T0.id_meeting, T0.idofmeeting, T0.topics_of_meeting, T0.start_date, T0.start_time, T0.end_time, T0.vanue, T0.apprv, T0.statuse, T1.fullname as nmfull, T2.id_notulen, T2.notulen ');
            $builder->join('users T1', 'T0.id = T1.id');
            $builder->join('meeting_notulen T2', 'T0.id_meeting= T2.id_meeting');
            $builder->havingIn('T0.id_meeting', $group);
            $query = $builder->get();
            return $query->getResult();
        }
    }

    public function notulenbefor($idofmeeting)
    {
        $builder = $this->db->table('meeting_schedule T0');
        $builder->select('T0.id_meeting, T0.topics_of_meeting, T0.start_date, T0.start_time, T0.end_time, T0.vanue, T0.apprv, T0.statuse, T1.fullname as nmfull, T2.id_notulen, T2.notulen ');
        $builder->join('users T1', 'T0.id = T1.id');
        $builder->join('meeting_notulen T2', 'T0.id_meeting= T2.id_meeting');
        $builder->where('T0.id_meeting', $idofmeeting);
        $query = $builder->get();
        return $query->getRow();
    }

    public function apprvschedule()
    {
        $builder = $this->db->table('meeting_schedule T0');
        $builder->select('T0.id_meeting, T0.topics_of_meeting, T0.start_date, T0.start_time, T0.end_time, T0.vanue, T0.apprv, T0.statuse, T1.fullname, T0.tools');
        $builder->join('users T1', 'T0.id = T1.id');
        $builder->where('T0.created_at >= DATE(NOW()) - INTERVAL 3 DAY');
        $query = $builder->get();
        return $query->getResult();
    }

    public function notifschedule()
    {
        $builder = $this->db->table('meeting_schedule T0');
        $builder->selectCount('T0.id_meeting');
        $builder->join('users T1', 'T0.id = T1.id');
        $builder->where('T0.created_at >= DATE(NOW()) - INTERVAL 3 DAY');
        $query = $builder->get();
        return $query->getRow();
    }

    public function ongoingschedule($id)
    {
        $builder = $this->db->table('meeting_schedule');
        $builder->select('id_meeting,start_date, topics_of_meeting');
        $builder->where('statuse', '2');
        $builder->where('apprv', '1');
        $builder->where('id', $id);
        $query = $builder->get();
        return $query->getResult();
    }

    public function attendance($id_meeting, $id)
    {
        $builder = $this->db->table('meeting_participant');
        $builder->select('id_kehadiran,id_meeting,id');
        $builder->where('id_meeting', $id_meeting);
        $builder->where('id', $id);
        $query = $builder->get();
        return $query->getResult();
    }
}
