<?php

namespace App\Models;

use CodeIgniter\Model;

class UserAccessModel extends Model
{
    protected $table = 'auth_groups';
    protected $useTimestamps = true;
    protected $allowedFields = [
        'name',
        'description'
    ];
    // protected $createdField = 'created_at';
    // protected $updatedField = 'updated_at';

    public function allgroup()
    {
        return $this->findAll();
    }

    public function access($id = null)
    {
        $builder = $this->db->table('auth_groups_users');
        $builder->select('*');
        $builder->join('auth_groups', 'auth_groups_users.group_id = auth_groups.id');
        $builder->where('auth_groups_users.user_id', $id);
        $query = $builder->get();
        return $query->getResult();
    }
}
