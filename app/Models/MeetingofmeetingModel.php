<?php

namespace App\Models;

use CodeIgniter\Model;

class MeetingofmeetingModel extends Model
{
    protected $table = 'meeting_of_meeting';
    protected $primaryKey = 'id_ofmeeting';
    protected $useTimestamps = true;
    protected $allowedFields = [
        'id_meeting',
        'id'
    ];
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';

}
