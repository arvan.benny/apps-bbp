<?php

namespace App\Models;

use CodeIgniter\Model;

class ParticipantModel extends Model
{
    protected $table = 'meeting_participant';
    protected $primaryKey = 'id_kehadiran';
    protected $useTimestamps = true;
    protected $allowedFields = [
        'id_meeting',
        'attend',
        'id',
        'status',
        'keterangan'
    ];
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';

}
