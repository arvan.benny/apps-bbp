<?php

namespace App\Models;

use CodeIgniter\Model;

class OperatorModel extends Model
{
    protected $table = 'table_inkomop';
    protected $useTimestamps = true;
    protected $allowedFields = [
        'id_inkomop',
        'inkomop'

    ];
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';

    public function all()
    {
        $builder = $this->db->table('table_inkomop');
        $builder->select('id_inkomop, inkomop',);
        $query = $builder->get();
        return $query->getResult();
    }

    public function getlistindikator($id_inkomop = null)
    {
        $builder = $this->db->table('table_list_indikator');
        $builder->select('id_listindikator,id_inkomop,listindikator,');
        $builder->where('id_inkomop',$id_inkomop);
        $query = $builder->get();
        return $query->getResult();
    }

    public function getdetailindikator($id_listindikator = null)
    {
        $builder = $this->db->table('table_detail_indikator');
        $builder->select('*');
        $builder->where('id_listindikator',$id_listindikator);
        $query = $builder->get();
        return $query->getResult();
    }
}
