<?php

namespace App\Models;

use CodeIgniter\Model;

class TestModel extends Model
{
    protected $table = 'OCRD';
    protected $useTimestamps = true;
    // protected $allowedFields = [
    //     'name',
    //     'description'
    // ];
    // protected $createdField = 'created_at';
    // protected $updatedField = 'updated_at';

    public function allgroup()
    {
        return $this->findAll();
    }
}
