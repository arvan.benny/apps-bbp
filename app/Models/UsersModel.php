<?php

namespace App\Models;

use CodeIgniter\Model;

class UsersModel extends Model
{
    protected $table = 'users';
    protected $useTimestamps = true;
    protected $allowedFields = [
        'id',
        'email',
        'username',
        'fullname',
        'password_hash',
        'position',
        'img',
        'nik',
        'id_dept'

    ];
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';

    public function getUser($id = null)
    {
        $builder = $this->db->table('users');
        $builder->select('users.id as userid, username, fullname, email, name, position, created_at, img, nik');
        $builder->join('auth_groups_users','auth_groups_users.user_id = users.id');
        $builder->join('auth_groups','auth_groups.id = auth_groups_users.group_id');
        $builder->where('users.id', $id);
        $query = $builder->get();
        return $query->getRow();
    }

    public function getRole($id = null)
    {
        $builder = $this->db->table('users');
        $builder->select('name');
        $builder->join('auth_groups_users','auth_groups_users.user_id = users.id');
        $builder->join('auth_groups','auth_groups.id = auth_groups_users.group_id');
        $builder->where('users.id', $id);
        $query = $builder->get();
        return $query->getResult();
    }

    public function allUser()
    {
        $builder = $this->db->table('users');
        $builder->select('users.id as userid, username, email');
        // $builder->join('auth_groups_users','auth_groups_users.user_id = users.id');
        // $builder->join('auth_groups','auth_groups.id = auth_groups_users.group_id');
        $query = $builder->get();
        return $query->getResult();
    }

    public function userIT()
    {
        $builder = $this->db->table('users');
        $builder->select('*');
        $builder->where('id_dept',1);
        $query = $builder->get();
        return $query->getResult();
    }

    public function userEXIM()
    {
        $builder = $this->db->table('users');
        $builder->select('*');
        $builder->where('id_dept',2);
        $query = $builder->get();
        return $query->getResult();
    }

    public function userFAT()
    {
        $builder = $this->db->table('users');
        $builder->select('*');
        $builder->where('id_dept',3);
        $query = $builder->get();
        return $query->getResult();
    }

    public function userGM()
    {
        $builder = $this->db->table('users');
        $builder->select('*');
        $builder->where('id_dept',4);
        $query = $builder->get();
        return $query->getResult();
    }

    public function userHRGA()
    {
        $builder = $this->db->table('users');
        $builder->select('*');
        $builder->where('id_dept',5);
        $query = $builder->get();
        return $query->getResult();
    }

    public function userOM()
    {
        $builder = $this->db->table('users');
        $builder->select('*');
        $builder->where('id_dept',6);
        $query = $builder->get();
        return $query->getResult();
    }

    public function userMarketing()
    {
        $builder = $this->db->table('users');
        $builder->select('*');
        $builder->where('id_dept',7);
        $query = $builder->get();
        return $query->getResult();
    }

    public function userPPIC()
    {
        $builder = $this->db->table('users');
        $builder->select('*');
        $builder->where('id_dept',8);
        $query = $builder->get();
        return $query->getResult();
    }

    public function userProduksi()
    {
        $builder = $this->db->table('users');
        $builder->select('*');
        $builder->where('id_dept',9);
        $query = $builder->get();
        return $query->getResult();
    }

    public function userPurch()
    {
        $builder = $this->db->table('users');
        $builder->select('*');
        $builder->where('id_dept',10);
        $query = $builder->get();
        return $query->getResult();
    }

    public function userQC()
    {
        $builder = $this->db->table('users');
        $builder->select('*');
        $builder->where('id_dept',11);
        $query = $builder->get();
        return $query->getResult();
    }

    public function userTeknik()
    {
        $builder = $this->db->table('users');
        $builder->select('*');
        $builder->where('id_dept',12);
        $query = $builder->get();
        return $query->getResult();
    }
    
    public function userWarehouse()
    {
        $builder = $this->db->table('users');
        $builder->select('*');
        $builder->where('id_dept',13);
        $query = $builder->get();
        return $query->getResult();
    }
    
}
