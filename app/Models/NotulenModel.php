<?php

namespace App\Models;

use CodeIgniter\Model;

class NotulenModel extends Model
{
    protected $table = 'meeting_notulen';
    protected $primaryKey = 'id_notulen';
    protected $useTimestamps = true;
    protected $allowedFields = [
        'id_notulen',
        'id',
        'id_meeting',
        'title',
        'notulen',
        'satat'
    ];
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';

}
