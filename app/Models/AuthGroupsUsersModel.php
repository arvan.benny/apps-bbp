<?php

namespace App\Models;

use CodeIgniter\Model;

class AuthGroupsUsersModel extends Model
{
    protected $table = 'auth_groups_users';
    // protected $useTimestamps = true;
    protected $allowedFields = [
        'group_id',
        'user_id'
    ];
    // protected $createdField = 'created_at';
    // protected $updatedField = 'updated_at';

}
